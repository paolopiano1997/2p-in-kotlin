package it.unibo.tuprolog.solve

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Term

/** A typealias to represent Prolog Flags, that are a mapping from atoms to terms */
typealias PrologFlags = Map<Atom, Term>
